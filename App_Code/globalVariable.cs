﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public static class globalVariable
{

   // static string _projectPath = @"C:\Users\Vikaro\OneDrive\PAWW.NET\ProjektPortal\Project\";
    static string _projectPath = HttpContext.Current.Request.MapPath("~/App_Data/");
    static string _databasePath = _projectPath+ "database.xml";
    static string _profilePath = _projectPath + "Profile/";
   

    public static string databasePath
    {
        get
        {
            return _databasePath;
        }
    }
    public static string projectPath
    {
        get { return _projectPath; }
    }
    public static string profilePath
    {
        get { return _profilePath; }
    }
}