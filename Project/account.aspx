﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Project/logged.master" AutoEventWireup="true" CodeFile="Account.aspx.cs" Inherits="Project_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="StyleSheet1.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceholder" runat="server">
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="placeholder" runat="server">
    <p>
        <asp:Image ID="ImageAccountProfile" runat="server" Height="155px" Width="154px" />
        <asp:Label CssClass="name" ID="LbAccountName" runat="server" Text="Imie Nazwisko"></asp:Label>
    </p>
<p id="paragraph">
        <asp:Button class="niceButton" ID="BtnAccountViewPodstawowe" runat="server" Text="Podstawowe Informacje" />
        <asp:Button class="niceButton" ID="BtnAccountViewZainteresowania" runat="server" Text="O mnie"  />
        <asp:Button class="niceButton" ID="BtnAccountViewOczekiwania" runat="server" Text="Oczekiwania" />
        <asp:Button class="niceButton" ID="BtnAccountViewKontakt" runat="server" Text="Kontakt" />
    </p>
    <br />
    <p class="View">

        <asp:MultiView ID="MlvAccount" runat="server">

            <asp:View ID="ViewPodstawowe" runat="server"> 
                <asp:Label CssClass="label" ID="Label5" runat="server" Text="Wiek: "> </asp:Label> <asp:Label CssClass="label" ID="LbAccountWiek" runat="server" Text="Label">XXX </asp:Label> <br />
                <asp:Label CssClass="label" ID="Label6" runat="server" Text="Miejscowość: "></asp:Label> <asp:Label CssClass="label" ID="LbAccountMiejscowosc" runat="server" Text="Label">XXX </asp:Label> <br />
                <asp:Label CssClass="label" ID="Label7" runat="server" Text="Label">Wykształcenie: </asp:Label> <asp:Label CssClass="label" ID="LbAccountWyksztalcenie" runat="server" Text="XXX"> </asp:Label>
            </asp:View>

            <asp:View ID="ViewDodatkowe" runat="server"> 
                <asp:Label CssClass="label" ID="Label1" runat="server" Text="Label">Wzrost: </asp:Label> <asp:Label CssClass="label" ID="LbAccountWzrost" runat="server" Text="Label">XXX </asp:Label> <br />
                <asp:Label CssClass="label" ID="Label2" runat="server" Text="Label">Kolor oczu: </asp:Label> <asp:Label CssClass="label" ID="LbAccountKolor" runat="server" Text="Label">XXX </asp:Label> <br />
                <asp:Label CssClass="label" ID="Label3" runat="server" Text="Label">Waga: </asp:Label> <asp:Label CssClass="label" ID="LbAccountWaga" runat="server" Text="Label">XXX </asp:Label> <br />
                <asp:Label CssClass="label" ID="Label4" runat="server" Text="Label">Stan cywilny: </asp:Label> <asp:Label CssClass="label" ID="LbAccountStanCyw" runat="server" Text="Label">XXX </asp:Label>
                
            </asp:View>

            <asp:View ID="ViewZainteresowania" runat="server"> 
                <asp:Label CssClass="label" ID="LbZainteresowania" runat="server" Text="Label">O mnie: </asp:Label><br />
                <asp:Label CssClass="label" ID="LbAccountZainteresowania" runat="server" Text="Label">XX <br /> XX <br /> XX </asp:Label>
            </asp:View>

            <asp:View ID="ViewOczekiwania" runat="server"> 
                <asp:Label CssClass="label" ID="Label8" runat="server" Text="Oczekiwania">Oczekiwania względem partnera:</asp:Label> <br />
                <asp:Label CssClass="label" ID="LbAccountOczekiwania" runat="server" Text="Label">XX <br /> XX <br /> XX </asp:Label>              
            </asp:View>

            <asp:View ID="ViewKontakt" runat="server"> 
                <asp:Label CssClass="label" ID="Label9" runat="server" Text="Label">Numer telefonu: </asp:Label> <asp:Label CssClass="label" ID="LbAccountNumer" runat="server" Text="Label">000-000-000 </asp:Label> <br />
                <asp:Label CssClass="label" ID="Label10" runat="server" Text="Label">Adres e-mail: </asp:Label><asp:Label CssClass="label" ID="LbAccountMail" runat="server" Text="Label">xxx@xx.xx </asp:Label>
            </asp:View>

        </asp:MultiView>
    </p>
</asp:Content>


