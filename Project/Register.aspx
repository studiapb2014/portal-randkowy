﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Project/unlogged.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Project_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>



<asp:Content ID="RegisterContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="registerbody">
    <div class="register">
        <table class="table">
            <tr>
                <td> <asp:Label ID="Label6" runat="server" Text="Podstawowe informacje: " Font-Size="X-Large"></asp:Label>  </td> 
            </tr>
            <tr> <td></td> </tr> <!-- ODSTĘP -->
            <tr>
                <td class="auto-style1"> <asp:Label ID="Label1" runat="server" Text="Imie:"></asp:Label> <br /> </td>
                <td class="auto-style1"> <asp:TextBox CssClass="textbox" ID="TbRegisterName" runat="server"></asp:TextBox> <br /></td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label2" runat="server" Text="Nazwisko: "></asp:Label> <br /></td>
                <td> <asp:TextBox CssClass="textbox" ID="TbRegisterSurname" runat="server"></asp:TextBox> <br /></td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label3" runat="server" Text="Wiek: "></asp:Label> <br /></td>
                <td> <asp:TextBox CssClass="textbox" ID="TbRegisterAge" runat="server"></asp:TextBox> <br /> </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label5" runat="server" Text="Miejscowość: "></asp:Label> </td>
                <td> <asp:TextBox CssClass="textbox" ID="TbRegisterCity" runat="server"></asp:TextBox> <br /></td>
            </tr>
            
            <tr> <td></td></tr> <!-- ODSTĘP -->
            <tr>
                <td> <asp:Label ID="Label7" runat="server" Text="Dodatkowe informacje: " Font-Size="X-Large"></asp:Label> <br /></td>
            </tr>
            <tr> <td></td></tr> <!-- ODSTĘP -->
            <tr>
                <td> <asp:Label ID="Label8" runat="server" Text="Wzrost: "></asp:Label></td>
                <td> <asp:TextBox CssClass="textbox" ID="TbRegisterWzrost" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label9" runat="server" Text="Kolor oczu: "></asp:Label></td>
                <td><asp:TextBox CssClass="textbox" ID="TbRegisterOczy" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label10" runat="server" Text="Waga:"></asp:Label></td>
                <td> <asp:TextBox CssClass="textbox" ID="TbRegisterWaga" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label4" runat="server" Text="Stan cywilny: "></asp:Label> <br /></td>
                <td>
                    <asp:DropDownList CssClass="select" ID="DDListViewDodatkowe" runat="server">
                        <asp:ListItem>Wolny/a</asp:ListItem>
                        <asp:ListItem>Rozwiedziony/a</asp:ListItem>
                        <asp:ListItem>Wdowiec</asp:ListItem>
                        <asp:ListItem>Wdowa</asp:ListItem>
                    </asp:DropDownList> 
                </td>
            </tr>
            
        </table>
        </div>
        
    <div class="register">
        <table class="table">
            <tr>
                <td> <asp:Label ID="Label11" runat="server" Text="Kilka słów o sobie: " Font-Size="Large"></asp:Label></td>
            </tr>
            <tr>
                <td> <asp:TextBox CssClass="textbox" ID="TbRegisterOSobie" runat="server" Height="100px" Width="184px" TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label12" runat="server" Text="Oczekiwania: " Font-Size="Large"></asp:Label></td>
            </tr>
            <tr>
                <td> <asp:TextBox CssClass="textbox" ID="TextBox1" runat="server" Height="100px" Width="184px" TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            </table>
        </div>
            <div class="register"> 
        <table class="table">
            <tr>
                <td> <asp:Label ID="Label13" runat="server" Text="Dane kontaktowe: " Font-Size="X-Large"></asp:Label></td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label15" runat="server" Text="Numer telefonu: "></asp:Label></td>
            </tr>
            <tr>
                <td><asp:TextBox CssClass="textbox" ID="TbRegisterNumer" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label14" runat="server" Text="Adres e-mail: "></asp:Label></td>
            </tr>
            <tr>
                <td> <asp:TextBox CssClass="textbox" ID="TbRegisterMail" runat="server"></asp:TextBox></td>
            </tr>
            <tr> <td></td></tr>
            <tr>
                <td>
                    <asp:Button CssClass="niceButton" ID="btnCancel" runat="server" Text="Anuluj" />
                    <asp:Button CssClass="niceButton" ID="btnProcess" runat="server" Text="Zatwierdź" />
                </td>

            </tr>

        </table>
            </div>
    </div>

    
</asp:Content>
