﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;

public partial class Project_Default2 : System.Web.UI.Page
{
     string FILE_NAME = globalVariable.databasePath;
    int ID;
    protected void Page_Load(object sender, EventArgs e)
    {       
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        XPathDocument doc;
        XPathNavigator nav;
        XPathExpression expr;
        XPathNodeIterator iterator;
        
        doc = new XPathDocument(FILE_NAME);
        nav = doc.CreateNavigator();
        expr = nav.Compile("/Accounts/ACC[login='" + txtBoxLogin.Text + "' and pass='" + txtBoxPassword.Text + "']");
        iterator = nav.Select(expr);
        if (iterator.MoveNext())
        {
            XPathNavigator nav2 = iterator.Current.Clone();
            ID = Int32.Parse(nav2.GetAttribute("ID", ""));
            nav2.MoveToFollowing("login", "");
            Label1.Text += nav2.Value;
            nav2.MoveToFollowing("pass", "");
            Label1.Text += nav2.Value;

            var url = "";
            url += "c2MyProfile.aspx?id=" + Server.UrlEncode(ID.ToString());
            Server.Transfer(url);
        }
    }
}